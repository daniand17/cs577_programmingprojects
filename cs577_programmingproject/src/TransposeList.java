public class TransposeList<T> implements IList<T> {

	private int ops = 0;
	private Listnode<T> head;
	private int size = 0;

	public TransposeList() {
		head = new Listnode<T>();
		head.setData(null);
		head.setNext(null);
	}

	@Override
	public void add(T t) {

		if ( t == null )
			return;

		Listnode<T> newNode = new Listnode<T>();
		newNode.setData(t);

		// Get to end of list
		Listnode<T> curr = head;
		while (curr.getNext() != null)
			curr = curr.getNext();
		// Link node
		newNode.setPrevious(curr);
		curr.setNext(newNode);
		size++;
	}

	@Override
	public boolean contains(T t) {

		Listnode<T> curr = head.getNext();
		// Loop through to find the value
		while (curr != null) {
			if ( curr.getData().equals(t) )
				return true;
			curr = curr.getNext();
		}
		return false;
	}

	@Override
	public T removeAt(int index) {

		if ( index >= size ) {
			System.out.println("Invalid index!");
			return null;
		}
		else {
			Listnode<T> curr = head.getNext();
			// Get to the index
			for (int i = 0; i < index; i++)
				curr = curr.getNext();
			// Unlink the current node
			if ( curr.getNext() != null )
				curr.getNext().setPrevious(curr.getPrevious());
			curr.getPrevious().setNext(curr.getNext());
			size--;
			return curr.getData();
		}
	}

	@Override
	public T getIndex(int index) {
		if ( index >= size ) {
			System.out.println("Invalid index!");
			return null;
		}
		else {
			Listnode<T> curr = head.getNext();
			for (int i = 0; i < index; i++)
				curr = curr.getNext();

			swap(curr.getPrevious(), curr);
			return curr.getData();
		}
	}

	@Override
	public T findItem(T t) {

		if ( size > 0 ) {
			Listnode<T> curr = head.getNext();

			while (curr != null) {

				if ( curr.getData().equals(t) ) {
					swap(curr.getPrevious(), curr);
					ops++;
					return curr.getData();
				}
				curr = curr.getNext();
				ops++;
			}
		}
		
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	public String toString() {

		if ( size == 0 ) {
			System.out.println("Empty list!");
			return null;
		}
		String str = "";
		Listnode<T> curr = head.getNext();
		while (curr != null) {
			str += curr.getData().toString();
			if ( curr.getNext() != null ) {
				str += "->";
			}
			curr = curr.getNext();
		}
		System.out.println(str);
		return str;
	}

	private void swap(Listnode<T> lhs, Listnode<T> rhs) {
		if ( lhs.equals(head) )
			return;
		// Set up lhs links
		lhs.getPrevious().setNext(rhs);
		if ( rhs.getNext() != null )
			rhs.getNext().setPrevious(lhs);
		lhs.setNext(rhs.getNext());
		rhs.setPrevious(lhs.getPrevious());
		rhs.setNext(lhs);
		lhs.setPrevious(rhs);
		ops++;
	}
	
	public int getOps(){
		return ops;
	}
}
