package randomWalkProject;

import java.util.Random;

public class RandomWalkSimulation1D {

	private float[] array;
	private int startIndex = 0;
	private Random rand;
	private float runsFailed = 0;

	public RandomWalkSimulation1D(int arrayLength, int startIndex) {
		if ( startIndex > arrayLength ) {
			System.err.println("Starting index must be less than array length!");
			System.exit(0);
		}
		array = new float[arrayLength];
		this.startIndex = startIndex;
		rand = new Random(System.nanoTime());
	}

	public float[] getArray() {
		return array;
	}

	public void runSimulation(int runs, int flips) {

		float[] res = new float[2];

		for (int i = 0; i < runs; i++) {

			float[] results = doRun(flips);
			res[0] += results[0];
			res[1] += results[1];
		}

		System.out.println("\n1-Dimensional Simulation:\nAverage Steps: " + (res[0] / runs)
				+ "\nAverage time: " + (res[1] / runs / 1000000f) + " ms"
				+ "\nPercent runs completed: " + (1 - runsFailed / runs) * 100f + "%");
	}

	// Returns steps as the first element and time as the second element
	private float[] doRun(int n) {

		double time = System.nanoTime();
		int stepCount = 0;

		int currIndex = startIndex;

		while (true) {
			// increment the position
			array[currIndex]++;
			// Flip the coin
			int randInt = rand.nextInt(2);
			if ( randInt == 0 )
				currIndex--;
			else
				currIndex++;
			// Stopping conditions
			stepCount++;
			if ( currIndex < 0 || currIndex >= array.length )
				break;

			if ( stepCount == n ) {
				runsFailed++;
				break;
			}

		}
		// Get the results
		time = System.nanoTime() - time;
		float[] results = { (float) stepCount, (float) time };
		return results;
	}
}
