package randomWalkProject;
/**
 * This class is a wrapper for an array of floats. Allows one to set a starting
 * point, and move a "cursor" through an array, drawing pixels to it. This data
 * can be normalized to show the pixel visited the most, and all other pixels
 * shown at a lesser shade to it.
 * 
 * @author Andrew Daniel
 *
 */
public class PixelArray {

	// The array of pixels to visit
	private float[][] pixels;
	// The current X and Y coordinates
	private int x = 0;
	private int y = 0;
	// Where the simulation started
	private int startX = 0;
	private int startY = 0;

	// Public constructor which takes a width and height in pixels
	public PixelArray(int width, int height) {
		// Create the array
		pixels = new float[height][width];
		// Initialize to 0 (pixel not visited)
		for (int i = 0; i < pixels.length; i++)
			for (int j = 0; j < pixels[i].length; j++)
				pixels[i][j] = 0.0f;
	}

	/**
	 * Get the dimensions of the pixel array
	 * 
	 * @return a 2 element array containing width [0] and height [1]
	 */
	public int[] dimensions() {
		int[] temp = { pixels.length, pixels[0].length };
		return temp;
	}

	/**
	 * Sets the starting position of the "cursor" in the array
	 * 
	 * @param x
	 *            the start x
	 * @param y
	 *            the start y
	 */
	public void setStartingPosition(int x, int y) {
		this.x = x;
		startX = x;
		this.y = y;
		startY = y;
	}

	/**
	 * Gets the starting position of the current run
	 * 
	 * @return a 2 element array containing the starting x (0) and the starting
	 *         y (1)
	 */
	public int[] getStartingPosition() {
		int[] temp = { startX, startY };
		return temp;
	}

	/**
	 * Moves the cursor up one pixel
	 * 
	 * @return whether a boundary has been touched
	 */
	public boolean moveUp() {
		incLocation();
		if ( y < pixels.length )
			y++;

		if ( y == pixels.length )
			return true;
		return false;
	}

	/**
	 * Moves the cursor down one pixel
	 * 
	 * @return whether a boundary has been touched
	 */
	public boolean moveDown() {
		incLocation();
		if ( y > 0 )
			y--;
		if ( y == 0 )
			return true;
		return false;
	}

	/**
	 * Moves the cursor right one pixel
	 * 
	 * @return whether a boundary has been touched
	 */
	public boolean moveRight() {
		incLocation();
		if ( x < pixels[y].length )
			x++;
		if ( x == pixels[y].length )
			return true;
		return false;
	}

	/**
	 * Moves the cursor left one pixel
	 * 
	 * @return whether a boundary has been touched
	 */
	public boolean moveLeft() {
		incLocation();
		if ( x > 0 )
			x--;
		if ( x == 0 )
			return true;
		return false;
	}

	/**
	 * Increment the current locations value (ie 1 -> 2) signifying it has been
	 * visited
	 * 
	 * @return whether the location has been incremented
	 */
	private boolean incLocation() {
		if ( x > 0 && x < pixels.length && y > 0 && y < pixels[x].length ) {
			pixels[x][y]++;
			if ( pixels[x][y] < 0 )
				pixels[x][y] = 0;
			return true;
		}
		return false;
	}

	/**
	 * Prints a summary of the pixel array
	 */
	public void printPixelArray() {
		for (int i = 0; i < pixels.length; i++) {
			for (int j = 0; j < pixels[i].length; j++)
				System.out.print(pixels[i][j] + " ");
			System.out.println();
		}
	}

	/**
	 * Get the max value of the pixel array, ie which pixel has been visited the
	 * most
	 * 
	 * @return the maximum value of the pixel array
	 */
	private float getMax() {

		float max = 0f;

		for (int i = 0; i < pixels.length; i++)
			for (int j = 0; j < pixels[i].length; j++)
				if ( pixels[i][j] > max )
					max = pixels[i][j];
		return max;
	}

	/**
	 * Gets a normalized version of the array. The pixel array is normalized
	 * with a normalization factor corresponding to the pixel which has been
	 * visited the most (the maximum value in the array)
	 * 
	 * @return the normalized array with maximum value = 1
	 */
	public float[][] normalizedArray() {
		// Find the max value
		float normFactor = this.getMax();
		// Normalize each element with the norming factor
		float[][] temp = new float[pixels.length][pixels[0].length];
		for (int i = 0; i < temp.length; i++)
			for (int j = 0; j < temp[i].length; j++)
				temp[i][j] = pixels[i][j] / normFactor;
		return temp;
	}
}