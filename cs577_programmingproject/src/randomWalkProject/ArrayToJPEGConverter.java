package randomWalkProject;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ArrayToJPEGConverter {

	private PixelArray pixArray;

	private BufferedImage img;

	public ArrayToJPEGConverter(PixelArray pixArray, String filename) {
		this.pixArray = pixArray;

		int[] dims = pixArray.dimensions();

		img = new BufferedImage(dims[0], dims[1], BufferedImage.TYPE_INT_RGB);
	}

	public void writeToJPEG() throws IOException {

		float[][] normArray = pixArray.normalizedArray();

		float maxRGB = 255;

		for (int i = 0; i < img.getWidth(); i++) {
			for (int j = 0; j < img.getHeight(); j++) {

				int col = (int) (255 - ((int) normArray[i][j] * maxRGB));

				int color = new Color(col, col, col).getRGB();

				img.setRGB(i, j, color);
			}
		}

		File outputFile = new File("image.png");

		if ( outputFile.canWrite() )
			System.out.println("Can write to file");
		if ( ImageIO.write(img, "png", outputFile) )
			System.out.println("File written");
	}
}
