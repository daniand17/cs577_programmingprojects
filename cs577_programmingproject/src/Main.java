import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

import randomWalkProject.ArrayToPPMConverter;
import randomWalkProject.RandomWalkSimulation1D;
import randomWalkProject.RandomWalkSimulation2D;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {

		// Starts a simulation
		// arg1 = dimensions of 2D array
		// arg2 = # of runs to perform
		// arg3 = # of flips before quit
		runRandomWalk2D(50, 10000, 600);

		// Starts a 1D simulation
		// arg1 = array length
		// arg2 = # runs to perform
		// arg3 = # flips before quit
		// arg4 = starting position
		runRandomWalk1D(100, 10, 927, 50);
		// runProject2Code();
	}

	private static void runProject2Code() throws FileNotFoundException {
		File infile = new File("webpage.txt");
		Scanner scanner = new Scanner(infile);

		String[] dataset = scanner.nextLine().split(",");
		int[] ints = new int[dataset.length];

		scanner.close();

		for (int i = 0; i < ints.length; i++)
			ints[i] = Integer.parseInt(dataset[i]);

		long countExecTime = 0;
		long mtfExecTime = 0;
		long transExecTime = 0;
		long llistExecTime = 0;

		int numIters = 10000;

		for (int iter = 0; iter < numIters; iter++) {

			CountList<Integer> countList = new CountList<Integer>();
			MoveToFrontList<Integer> mtfList = new MoveToFrontList<Integer>();
			TransposeList<Integer> transList = new TransposeList<Integer>();
			LinkedList<Integer> llist = new LinkedList<Integer>();

			for (int i = 0; i < 105; i++) {
				countList.add(i);
				mtfList.add(i);
				transList.add(i);
				llist.add(i);
			}
			/*
			 * for (int i = 0; i < 50; i++) { countList.add(i); mtfList.add(i);
			 * transList.add(i); llist.add(i); }
			 */

			long time = System.nanoTime();
			for (int i = 0; i < ints.length; i++) {
				countList.findItem(ints[i]);
			}

			countExecTime += System.nanoTime() - time;

			time = System.nanoTime();
			for (int i = 0; i < ints.length; i++) {
				mtfList.findItem(ints[i]);
			}
			mtfExecTime += System.nanoTime() - time;

			time = System.nanoTime();
			for (int i = 0; i < ints.length; i++) {
				transList.findItem(ints[i]);
			}

			transExecTime += System.nanoTime() - time;

			time = System.nanoTime();
			for (int i = 0; i < ints.length; i++)
				llist.contains(ints[i]);

			llistExecTime += System.nanoTime() - time;
		}
		countExecTime /= numIters;
		mtfExecTime /= numIters;
		transExecTime /= numIters;
		llistExecTime /= numIters;

		System.out.println("Times in ns, " + "Num Iters: " + numIters + "\nCount: " + countExecTime
				+ "\nMTF: " + mtfExecTime + "\nTrans: " + transExecTime + "\nLList: "
				+ llistExecTime);
	}

	public static void runRandomWalk2D(int dims, int runs, int flips) {
		// Creates a new simulation with specified size in pixels
		RandomWalkSimulation2D sim = new RandomWalkSimulation2D(dims, dims);
		// Run the simulation passing the # of runs to do
		sim.runSimulation(runs, flips);
		// Create the object which will convert the pixel array to a PPM file
		ArrayToPPMConverter ppmConv = new ArrayToPPMConverter(sim.getPixelArray(), "2DHeatmap");
		// Write the data to a PPM file
		ppmConv.writeToPPM();
	}

	public static void runRandomWalk1D(int arrayLength, int runs, int flips, int start) {

		RandomWalkSimulation1D sim1D = new RandomWalkSimulation1D(arrayLength, start);
		sim1D.runSimulation(runs, flips);

		float[] theArray = sim1D.getArray();

		System.out.println("Average # times each cell was visited over " + runs + " runs: ");
		for (int i = 0; i < theArray.length; i++) {
			System.out.print("[" + theArray[i] / runs + "]");
			if ( i % 10 == 0 )
				System.out.println();
		}
	}
}
