public class CountList<T> implements IList<T> {

	private int ops = 0;
	private Listnode<T> head;
	private int size = 0;

	public CountList() {
		head = new Listnode<T>();
		head.setData(null);
		head.setNext(null);
	}

	@Override
	public void add(T t) {
		// Create the new node and set its data
		Listnode<T> newNode = new Listnode<T>();
		newNode.setData(t);

		Listnode<T> curr = head;
		// Get to the end of the list
		while (curr.getNext() != null)
			curr = curr.getNext();
		// Link node to the end of the list
		newNode.setPrevious(curr);
		curr.setNext(newNode);
		size++;
	}

	@Override
	public boolean contains(T t) {

		Listnode<T> curr = head.getNext();

		while (curr != null) {

			if (curr.getData().equals(t))
				return true;

			curr = curr.getNext();
		}
		return false;
	}

	@Override
	public T removeAt(int index) {
		Listnode<T> curr = head.getNext();
		Listnode<T> prev = head;
		for (int i = 0; i < index; i++) {
			prev = curr;
			curr = curr.getNext();
		}
		// Set up the links
		prev.setNext(curr.getNext());
		if (curr.getNext() != null)
			curr.getNext().setPrevious(prev);
		size--;
		return curr.getData();
	}

	@Override
	public T getIndex(int index) {

		if (index >= size)
			return null;

		Listnode<T> curr = head.getNext();
		for (int i = 0; i < index; i++)
			curr = curr.getNext();

		T data = curr.getData();
		curr.incrementCount();
		if (index > 0 && curr.getCount() > curr.getPrevious().getCount())
			bubbleUp(curr.getPrevious(), curr);
		return data;
	}

	@Override
	public T findItem(T t) {

		if (size == 0)
			return null;

		Listnode<T> curr = head.getNext();

		while (curr != null) {
			if (curr.getData().equals(t)) {
				T data = curr.getData();
				curr.incrementCount();
				if (!curr.getPrevious().equals(head)
						&& curr.getCount() > curr.getPrevious().getCount())
					bubbleUp(curr.getPrevious(), curr);
				ops++;
				return data;
			}
			curr = curr.getNext();
			ops++;
		}
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	private void bubbleUp(Listnode<T> lhs, Listnode<T> rhs) {

		while (!lhs.equals(head) && rhs.getCount() > lhs.getCount()) {
			lhs.getPrevious().setNext(rhs);
			if (rhs.getNext() != null)
				rhs.getNext().setPrevious(lhs);
			lhs.setNext(rhs.getNext());
			rhs.setPrevious(lhs.getPrevious());
			rhs.setNext(lhs);
			lhs.setPrevious(rhs);

			lhs = rhs.getPrevious();
			ops++;
		}
	}

	public String toString() {

		if (size == 0) {
			System.out.println("Empty list!");
			return "";
		}
		String msg = "";
		Listnode<T> curr = head.getNext();

		while (curr != null) {

			msg += curr.getData().toString() + "(" + curr.getCount() + ")";
			if (curr.getNext() != null) {

				msg += "->";
			}
			curr = curr.getNext();
		}

		System.out.println(msg);
		return msg;
	}

	public int getOps() {
		return ops;
	}
}
