public class MoveToFrontList<T> implements IList<T> {

	
	private int ops = 0;
	
	private Listnode<T> head;
	private int size = 0;

	public MoveToFrontList() {
		head = new Listnode<T>();
		head.setData(null);
		head.setNext(null);
	}

	public void add(T t) {

		// Create node and set data
		Listnode<T> temp = new Listnode<T>();
		temp.setData(t);
		// Set up the links
		temp.setNext(head.getNext());
		head.setNext(temp);
		size++;
	}

	public boolean contains(T t) {

		Listnode<T> curr = head.getNext();

		while (curr != null) {
			if ( curr.getData().equals(t) )
				return true;
			curr = curr.getNext();
		}
		return false;
	}

	public T removeAt(int index) {

		if ( index >= size )
			return null;

		Listnode<T> temp = head.getNext();
		Listnode<T> prev = head;

		int count = 0;
		// Find the node
		while (temp != null) {

			if ( count == index ) {
				prev.setNext(temp.getNext());
				size--;
				return temp.getData();
			}
			prev = prev.getNext();
			temp = temp.getNext();
			count++;
		}

		return null;
	}

	public T getIndex(int index) {
		if ( index >= size )
			return null;

		Listnode<T> curr = head.getNext();
		Listnode<T> prev = head;
		int count = 0;

		while (curr != null) {

			if ( count == index ) {
				prev.setNext(curr.getNext());
				curr.setNext(head.getNext());
				head.setNext(curr);
				return curr.getData();
			}
			prev = curr;
			curr = curr.getNext();
			count++;
		}

		return null;
	}

	public T findItem(T t) {

		Listnode<T> curr = head.getNext();
		Listnode<T> prev = head;

		while (curr != null) {

			if ( curr.getData().equals(t) ) {
				// Move to front
				prev.setNext(curr.getNext());
				curr.setNext(head.getNext());
				head.setNext(curr);
				ops++;
				return curr.getData();
			}

			prev = curr;
			curr = curr.getNext();
			ops++;
		}
		return null;
	}

	public int size() {
		return size;
	}

	public String toString() {

		String str = "";
		Listnode<T> curr = head.getNext();

		if ( head.getNext() == null )
			return "Empty list!";

		while (curr != null) {
			str += curr.getData().toString();
			if ( curr.getNext() != null )
				str += "->";
			curr = curr.getNext();
		}
		return str;
	}
	
	public int getOps(){
		return ops;
	}
}
